'''Using three tables, as done below, made the back-end much easier.
We were able to reference multiple tables, one for each model, in our database
and thus able to refer to related objects more easily.
'''

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgres://postgres:biediger10@localhost/bookdb')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True # to suppress a warning message
db = SQLAlchemy(app)

class Book(db.Model):
	__tablename__ = 'book'
	title = db.Column(db.String(80), nullable = False)
	id = db.Column(db.String(80), primary_key = True)
	description = db.Column(db.String(4000), nullable = True)
	image_url = db.Column(db.String(1000), nullable = True)
	publication_date = db.Column(db.String(1000), nullable=True)
	publishers = db.Column(db.PickleType())
	authors = db.Column(db.PickleType())

class Author(db.Model):
	__tablename__ = 'author'
	born = db.Column(db.String(1000), primary_key = True)
	name = db.Column(db.String(1000), nullable = False)
	education = db.Column(db.String(1000), nullable = True)
	nationality = db.Column(db.String(1000), nullable = True)
	description = db.Column(db.String(1000), nullable = True)
	alma_mater = db.Column(db.String(1000), nullable = True)
	wikipedia_url = db.Column(db.String(1000), nullable = True)
	image_url = db.Column(db.String(1000), nullable = True)

class Publisher(db.Model):
	__tablename__ = 'publisher'
	wikipedia_url = db.Column(db.String(1000), nullable = False)
	name = db.Column(db.String(1000), primary_key = True)
	description = db.Column(db.String(1000), nullable = False)
	owner = db.Column(db.String(1000), nullable = True)
	image_url = db.Column(db.String(1000), nullable = False)
	website = db.Column(db.String(1000), nullable = False)
	founded = db.Column(db.String(1000), nullable = False)
	location = db.Column(db.String(1000), nullable = False)
	parent = db.Column(db.String(1000), nullable = False)



db.drop_all()
db.create_all()

# End of models.py
