# beginning of create_db.py
import json
from models import app, db, Book, Author, Publisher


def load_json(filename):
    with open(filename) as file:
        jsn = json.load(file)
        file.close()

    return jsn

def create_books():
    book = load_json('books.json')
    for oneBook in book:
        # print(oneBook)
        title = oneBook['title']
        try:
            id = oneBook['isbn']
        except:
            id = oneBook['google_id']
        try:
            publication_date = oneBook['publication_date']
        except:
            publication_date = ''
        try:
            image_url = oneBook['image_url']
        except:
            image_url = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAflBMVEX/xJz////bmWyhakr/1bigaEefZkS3kX2oeFydYj/TvrOaY0P5v5f/yKLal2nosIqudVKbXjjt5eGocVHgp4PIiWGoc1T/277gs5fn29XNs6XCoZCaXDb59fO5k3+wg2vdzcXZxbvFp5fPt6qxhW7q39rTk2n28e67f1rRmXacEEpjAAAC7ElEQVR4nO3YbXeaMBiAYYEFja6bkdXON9B1uu7//8FJ7Npz1hP0kHRPQu/7a9qe52oCgqNs6I2kB3j3EKYfwvRDmH4I0w/hS7v9uq7HMbf3Eh4aY3QZdWbqITwUWhWxp/sLd42J3+cjXJSl9PA31Vt43L5sYGUrqmVbFVM+wl/PV+Bsfp9f+vL1oe0uokYzD+HkckSrU56/Cj+duxvF02cP4dRY4DzPhyrUFviUD1b4aIVVPlzh0t5mTsMVLrb/XoRDE0712y0clnBjPyryAQsb9faQDks4QRhJCBEilA8hQoTyIUSIUD6ECBHKhxAhQvkQIkQoH0KECOVDiBChfAgRIpQPIUKE8iFEiFA+hAgRyocQIUL5ECJEKB9ChAjlQ4gQoXwIESKUDyFChPIhRIhQPoQIEcqHECFC+RAiRCgfQoQI5UOIEKF8CBEilA8hQoTyIUSIUD6ECBHKhxAhQvkQIkQoH0KECOVDiBChfAgRIpQPIUKE8iFEiFA+hAgRyocQIUL5ECJEKB9ChB9U+BCR8C6M8HT/t9/f277FlLfw9FQVr6lZdBWewvmsSKD+wurq346j/sJU8hWW2kSbDiAs9Wa/WsTayhK3Kw+hqXddPybduhWWG8fqLcLtIfxUIVu2YxrXJtwg1Ot3mCpgi23XFt4gVE34oYI2bQ+pcVyFtwi183cjaVO2UzqXrwtN8JEC15ynV'
        try:
            description = oneBook['description']
        except:
            description = ''

        publishers = oneBook['publishers']
        authors = oneBook['authors']

        newBook = Book(title=title, id=id, description=description, image_url=image_url,
                       publishers=publishers, authors=authors, publication_date = publication_date)

        # After I create the book, I can then add it to my session.
        db.session.add(newBook)
        # commit the session to my DB.
        try:
            db.session.commit()
        except:
            pass



def create_authors():
    keyList = []
    book = load_json('books.json')
    for oneBook in book:
        name = oneBook['authors'][0]['name']

        try:
            born = oneBook['authors'][0]['born']
        except:
            born = ''

        if born in keyList:
            continue
        else:
            keyList.append(born)

        try:
            education = oneBook['authors'][0]['education']
        except:
            education = ''
        try:
            nationality = oneBook['authors'][0]['nationality']
        except:
            nationality = ''
        try:
            alma_mater = oneBook['authors'][0]['alma_mater']
        except:
            alma_mater = ''
        try:
            description = oneBook['authors'][0]['description']
        except:
            description = ''
        try:
            wikipedia_url = oneBook['authors'][0]['wikipedia_url']
        except:
            wikipedia_url = ''
        try:
            image_url = oneBook['authors'][0]['image_url']
        except:
            image_url = 'https://cms.qz.com/wp-content/uploads/2017/03/twitter_egg_blue.png?w=410&h=231&strip=all&quality=75'

        newAuthor = Author(name=name, born=born, education=education, nationality=nationality, alma_mater=alma_mater,#
                                    description=description, wikipedia_url=wikipedia_url, image_url=image_url)
        db.session.add(newAuthor)
         # commit the session to my DB.
        db.session.commit()



def create_publishers():
    keyList = []
    book = load_json('books.json')
    for oneBook in book:

        name = oneBook['publishers'][0]['name']
        if name in keyList:
            continue

        else:
            keyList.append(name)
        try:
            founded = oneBook['publishers'][0]['founded']
        except:
            founded = ''
        try:
            location = oneBook['publishers'][0]['location']
        except:
            location = ''
        try:
            wikipedia_url = oneBook['publishers'][0]['wikipedia_url']
        except:
             wikipedia_url = ''
        try:
            description = oneBook['publishers'][0]['description']
        except:
            description = ''
        try:
            parent = oneBook['publishers'][0]['parent company']
        except:
            parent = ''
        try:
            website = oneBook['publishers'][0]['website']
        except:
            website = ''
        try:
            image_url = oneBook['publishers'][0]['image_url']
        except:
            image_url = 'https://cms.qz.com/wp-content/uploads/2017/03/twitter_egg_blue.png?w=410&h=231&strip=all&quality=75'#

        #print(name, founded, location, parent, website, description, wikipedia_url, image_url)

        newPublisher = Publisher(name=name, founded=founded, location=location, parent=parent, website=website,
                                  description=description, wikipedia_url=wikipedia_url, image_url=image_url)
        db.session.add(newPublisher)
         # commit the session to my DB.

        db.session.commit()


create_books()
create_authors()
create_publishers()

# end of create_db.py
