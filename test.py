import os
import sys
import unittest
from models import db, Book, Author, Publisher
class DBTestCases(unittest.TestCase):
    def test_source_insert_1(self):
        s = Book(title = 'C++', id='20', description = '', publication_date = '', image_url ='', publishers = [], authors =[])
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Book).filter_by(id = '20').one()
        self.assertEqual(str(r.id), '20')

        db.session.query(Book).filter_by(id = '20').delete()
        db.session.commit()

    def test_source_insert_2(self):

        s = Book(title = 'C++', id='', description = 'Harry Potter', publication_date = '', image_url ='', publishers = [], authors =[])
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Book).filter_by(description = 'Harry Potter').one()
        self.assertEqual(str(r.description), 'Harry Potter')

        db.session.query(Book).filter_by(description = 'Harry Potter').delete()
        db.session.commit()
    
    def test_source_insert_3(self):
        s = Book(title = 'C++', id='', description = '', publication_date = '1800-12-08', image_url ='', publishers = [], authors =[])
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Book).filter_by(publication_date = '1800-12-08').one()
        self.assertEqual(str(r.publication_date), '1800-12-08')

        db.session.query(Book).filter_by(publication_date = '1800-12-08').delete()
        db.session.commit()

    def test_source_insert_4(self):
        s = Book(title = 'C++', id='', description = '', publication_date = '', image_url ='https://books.google.com/', publishers = [], authors =[])
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Book).filter_by(image_url = 'https://books.google.com/').one()
        self.assertEqual(str(r.image_url), 'https://books.google.com/')

        db.session.query(Book).filter_by(image_url = 'https://books.google.com/').delete()
        db.session.commit()
    
    def test_source_insert_5(self):
        s = Book(title = 'C++', id='', description = '', publication_date = '', image_url ='', 
                publishers = [
            'bestpublishers ever'
        ], authors =[])
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Book).filter_by(publishers = [
            'bestpublishers ever'
        ]).one()
        self.assertEqual((r.publishers), [
           'bestpublishers ever'
        ])

        db.session.query(Book).filter_by(publishers = [
         'bestpublishers ever'
        ]).delete()
        db.session.commit()
    
    def test_source_insert_6(self):
        s = Book(title = 'C++', id='', description = '', publication_date = '', image_url ='', publishers = [], authors = [
            'best author ever', 'author2'
        ])
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Book).filter_by(authors = [
            'best author ever', 'author2'
        ]).one()
        self.assertEqual((r.authors), [
            'best author ever', 'author2'
            
        ])

        db.session.query(Book).filter_by(authors = [
            'best author ever', 'author2'
        ]).delete()
        db.session.commit()
        

    def test_source_insert_8(self):
        s = Author(born = 'everyday-is-my-birthday', name = 'will.i.am biediger', education = 'B.S. Mathematics from UT Austin',nationality = 'American', description = 'awesomeness', alma_mater = 'UT Austin', wikipedia_url = 'https://en.wikipedia.org/wiki/Will.i.am', image_url= 'https://images.tmz.com/2013/04/23/0423-will-i-am-tmz-3.jpg')
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Author).filter_by(born = 'everyday-is-my-birthday').one()
        self.assertEqual((r.born), 'everyday-is-my-birthday')
        db.session.query(Author).filter_by(born = 'everyday-is-my-birthday').delete()
        db.session.commit()
	
    def test_source_insert_9(self):
        s = Author(born = '1948-12-08', name = 'Luis DAWG Caffarelli', education = 'Ph.D. yep he is smart',nationality = 'Argentinian', description = 'math genius but vague af', alma_mater = 'University of Buenos Aires', wikipedia_url = 'https://en.wikipedia.org/wiki/Luis_Caffarelli', image_url= 'https://memecrunch.com/meme/34B7S/real-analysis-cat/image.jpg?w=625&c=1')
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Author).filter_by(image_url= 'https://memecrunch.com/meme/34B7S/real-analysis-cat/image.jpg?w=625&c=1').one()
        self.assertEqual((r.image_url), 'https://memecrunch.com/meme/34B7S/real-analysis-cat/image.jpg?w=625&c=1')
        db.session.query(Author).filter_by(image_url= 'https://memecrunch.com/meme/34B7S/real-analysis-cat/image.jpg?w=625&c=1').delete()
        db.session.commit()
		
    def test_source_insert_10(self):
        s = Publisher(wikipedia_url = 'https://www.youtube.com/watch?v=dQw4w9WgXcQ', name = 'WEESS Publishers', description = 'Will, Erin, Emma, Son, and Shirley were so inspired by this project to start their own publishing company. Accepting donations now on kickstart', owner = 'will.i.am. biediger', image_url = 'https://i.pinimg.com/236x/0a/29/56/0a29565faf572bedb88da623e8a27d28--golden-girls-meme-library-memes.jpg', website = 'https://cs329e-idb-234220.appspot.com/', founded = '2019', location = 'Deep in the Heart of Texas', parent = 'we are adults now')
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Publisher).filter_by(location= 'Deep in the Heart of Texas').one()
        self.assertEqual((r.location), 'Deep in the Heart of Texas')
        db.session.query(Publisher).filter_by(location= 'Deep in the Heart of Texas').delete()
        db.session.commit()
		
    def test_source_insert_11(self):
        s = Publisher(wikipedia_url = 'https://en.wikipedia.org/wiki/Mathematics', name = 'The Mathematics Peeps', description = 'The people of the mathematics community have maintained a hidden publishing company for 3 centuries. Note: the spelling in these publications is kinda sketch.', owner = 'the math gods', image_url = 'https://www.reasonablefaith.org/images/rf_podcasts/rf_detail_261_0.jpg', website = 'http://www.math.com/', founded = '1800s', location = 'Math is everywhere', parent = 'none')
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Publisher).filter_by(owner= 'the math gods').one()
        self.assertEqual((r.owner), 'the math gods')
        db.session.query(Publisher).filter_by(owner= 'the math gods').delete()
        db.session.commit()

    def test_source_insert_12(self):
        s = Publisher(wikipedia_url = 'https://en.wikipedia.org/wiki/National_Security_Agency', name = 'Presidential Publishing', description = 'Coming soon, biographies of U.S. Presidents & other foreign leaders', owner = 'NSA---shhhh', image_url = 'https://pics.me.me/the-history-of-us-presidents-9-g-9-9-%E4%B8%80-12568340.png', website = 'https://www.whitehouse.gov/', founded = '1789', location = 'Washington, D.C.', parent = 'Great Britain')
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Publisher).filter_by(parent = 'Great Britain').one()
        self.assertEqual((r.parent), 'Great Britain')
        db.session.query(Publisher).filter_by(parent = 'Great Britain').delete()
        db.session.commit()
		
    def test_source_delete_3(self):
        exists = db.session.query(db.exists().where(Book.publication_date == '1800-12-08')).scalar()
        self.assertFalse(exists)
		
    def test_source_delete_1(self):
        exists = db.session.query(db.exists().where(Book.id == '20')).scalar()
        self.assertFalse(exists)

    def test_source_delete_2(self):
        exists = db.session.query(db.exists().where(Book.description == 'Harry Potter')).scalar()
        self.assertFalse(exists)
		
    def test_source_delete_4(self):
        exists = db.session.query(db.exists().where(Book.image_url == 'https://books.google.com/')).scalar()
        self.assertFalse(exists)
		
    def test_source_delete_5(self):
        exists = db.session.query(db.exists().where(Book.publishers == ['bestpublishers ever'])).scalar()
        self.assertFalse(exists)
        
    def test_source_delete_6(self):
        exists = db.session.query(db.exists().where(Book.authors == ['best author ever', 'author2'])).scalar()
        self.assertFalse(exists)

    def test_source_delete_7(self):
        exists = db.session.query(db.exists().where(Author.name == 'Erin Maes')).scalar()
        self.assertFalse(exists)
		
    def test_source_delete_8(self):
        exists = db.session.query(db.exists().where(Author.born == 'everyday-is-my-birthday')).scalar()
        self.assertFalse(exists)
		
    def test_source_delete_9(self):
        exists = db.session.query(db.exists().where(Author.image_url == 'https://memecrunch.com/meme/34B7S/real-analysis-cat/image.jpg?w=625&c=1')).scalar()
        self.assertFalse(exists)
		
    def test_source_delete_10(self):
        exists = db.session.query(db.exists().where(Publisher.location == 'Deep in the Heart of Texas')).scalar()
        self.assertFalse(exists)

    def test_source_delete_11(self):
        exists = db.session.query(db.exists().where(Publisher.owner == 'the math gods')).scalar()
        self.assertFalse(exists)	

    def test_source_delete_12(self):
        exists = db.session.query(db.exists().where(Publisher.parent == 'Great Britain')).scalar()
        self.assertFalse(exists)		
#Create rows, delete rows

if __name__ == '__main__':
    unittest.main()

